
# ProjectorUtils

This project contains scripts to help with managing projectors.  Only one projector model is currently supported, but I hope to expand the program to be more generally useful.  Contact me if you want to add support for your projectors.

The scripts in this project will need to be configured for your site, so before running them, make a copy, open it up, and fill in the Configuration section variables.


## Supported Projectors

* JVC DLA-SH7NLG


## check-lamp-times

This utility checks the lamp times of projectors, and if more than 100 hours has elapsed since the last time a notification was issued, issue a notification to a Slack webhook reporting the lamp times.

Bash and PowerShell versions of the script are provided.  Future development will probably focus on the PowerShell version.

### PowerShell Note

To make a launcher for use in your system startup:

 - Create a shortcut to check-lamp-times.ps1
 - Edit its target to be like:

```
C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -ExecutionPolicy Bypass -File "C:\Path\To\ProjectorUtils\check-lamp-times.ps1"
```
