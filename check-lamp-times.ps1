
###
### Configuration
###

$slack_webhook_url = ""

$projector1_ip = ""
$projector1_username = ""
$projector1_password = ""

$projector2_ip = ""
$projector2_username = ""
$projector2_password = ""

$log_file = "./check-lamp-times.log"


###
### Utilities
###

function Set-UseUnsafeHeaderParsing
{
    param(
        [Parameter(Mandatory,ParameterSetName='Enable')]
        [switch]$Enable,

        [Parameter(Mandatory,ParameterSetName='Disable')]
        [switch]$Disable
    )

    $ShouldEnable = $PSCmdlet.ParameterSetName -eq 'Enable'

    $netAssembly = [Reflection.Assembly]::GetAssembly([System.Net.Configuration.SettingsSection])

    if($netAssembly)
    {
        $bindingFlags = [Reflection.BindingFlags] 'Static,GetProperty,NonPublic'
        $settingsType = $netAssembly.GetType(
            'System.Net.Configuration.SettingsSectionInternal')

        $instance = $settingsType.InvokeMember('Section', $bindingFlags, $null, $null, @())

        if($instance)
        {
            $bindingFlags = 'NonPublic','Instance'
            $useUnsafeHeaderParsingField = $settingsType.GetField(
                'useUnsafeHeaderParsing', $bindingFlags)

            if($useUnsafeHeaderParsingField)
            {
              $useUnsafeHeaderParsingField.SetValue($instance, $ShouldEnable)
            }
        }
    }
}

function Convert-Minutes-to-HM ($min) {
    $h = [math]::floor($min / 60)
    $m = $min % 60
    "{0}:{1:d2}" -f $h, $m
}

function Convert-Minutes-to-Hundreds-of-Hours ($min) {
    [math]::floor($min / 6000)
}

function Submit-Slack-Post ($text) {
    $body = ConvertTo-Json @{'text' = $text}
    Invoke-RestMethod `
      -Uri $slack_webhook_url `
      -Method POST `
      -ContentType "application/json" `
      -Body $body
}

function Get-ProjectorInfo ($ip, $login, $password) {
    $LoginResponse = Invoke-WebRequest "http://$ip/cgi-bin/html.cgi" `
        -SessionVariable 'Session' -Body "name=$login&pass=$password&SET=LOGIN" `
        -Method 'POST'
    Start-Sleep -s 1
    $Response = Invoke-WebRequest "http://$ip/cgi-bin/web.cgi?power=q&err=q&input=q&temp=q&lamp_info=q&lamp_mode=q&display(mute=q)&_='" `
        -WebSession $Session
    [xml]$xmlfile = $Response.Content
    $xmlfile.projector
}


###
### Main
###

Set-UseUnsafeHeaderParsing -Enable

[Net.ServicePointManager]::SecurityProtocol = `
  [Net.SecurityProtocolType]::Tls `
  -bor [Net.SecurityProtocolType]::Tls11 `
  -bor [Net.SecurityProtocolType]::Tls12


$prev_info = @{ 'error_count' = 0 }
If (Test-Path $log_file) {
    Get-Content $log_file | Foreach-Object{
        if (! $_.StartsWith('#')) {
            $var = $_.Split('=')
            If ($var[0] -ne '') {
                $val = $var[1].Trim('"')
                $prev_info[ $var[0] ] = [int]$val
            }
        }
    }
}


$projector_info1 = Get-ProjectorInfo $projector1_ip $projector1_username $projector1_password
$p1_l1_time = $projector_info1.lamp_info.lamp
$p1_l2_time = $projector_info1.lamp_info.lamp2

$projector_info2 = Get-ProjectorInfo $projector2_ip $projector2_username $projector2_password
$p2_l1_time = $projector_info2.lamp_info.lamp
$p2_l2_time = $projector_info2.lamp_info.lamp2

If (($projector_info1.lamp_info.get -ne 'ok') -or
    ($projector_info2.lamp_info.get -ne 'ok'))
{
    $prev_info['error_count'] ++
}
Else
{
    $prev_info['error_count'] = 0
}

$p1_l1_hm = Convert-Minutes-to-HM $p1_l1_time
$p1_l2_hm = Convert-Minutes-to-HM $p1_l2_time
$p2_l1_hm = Convert-Minutes-to-HM $p2_l1_time
$p2_l2_hm = Convert-Minutes-to-HM $p2_l2_time

Write-Host "Projector 1:"
Write-Host ("  Lamp1 Time: {0}" -f $p1_l1_hm)
Write-Host ("  Lamp2 Time: {0}" -f $p1_l2_hm)
Write-Host "Projector 2:"
Write-Host ("  Lamp1 Time: {0}" -f $p2_l1_hm)
Write-Host ("  Lamp2 Time: {0}" -f $p2_l2_hm)

If ($prev_info['error_count'] -eq 0) {
    $p1_l1_htime = Convert-Minutes-to-Hundreds-of-Hours $p1_l1_time
    $p1_l2_htime = Convert-Minutes-to-Hundreds-of-Hours $p1_l2_time
    $p2_l1_htime = Convert-Minutes-to-Hundreds-of-Hours $p2_l1_time
    $p2_l2_htime = Convert-Minutes-to-Hundreds-of-Hours $p2_l2_time
} Else {
    $p1_l1_htime = $prev_info['prev_p1_l1_htime']
    $p1_l2_htime = $prev_info['prev_p1_l2_htime']
    $p2_l1_htime = $prev_info['prev_p2_l1_htime']
    $p2_l2_htime = $prev_info['prev_p2_l2_htime']
}

$logstr = @"
##
## Log created: $(Get-Date -format o)
##
## Projector 1:
##   Lamp1 Time: $p1_l1_hm
##   Lamp2 Time: $p1_l2_hm
## Projector 2:
##   Lamp1 Time: $p2_l1_hm
##   Lamp2 Time: $p2_l2_hm
##
prev_p1_l1_htime=$p1_l1_htime
prev_p1_l2_htime=$p1_l2_htime
prev_p2_l1_htime=$p2_l1_htime
prev_p2_l2_htime=$p2_l2_htime
error_count=$($prev_info['error_count'])
"@

$logstr | Out-File $log_file

If ($prev_info['error_count'] -gt 5)
{
    Submit-Slack-Post ("Projectors lamp info: error count: {0}" -f $prev_info['error_count'])
}
ElseIf (($p1_l1_htime -ne $prev_info['prev_p1_l1_htime']) -or
        ($p1_l2_htime -ne $prev_info['prev_p1_l2_htime']) -or
        ($p2_l1_htime -ne $prev_info['prev_p2_l1_htime']) -or
        ($p2_l2_htime -ne $prev_info['prev_p2_l2_htime']))
{
    ## log it!
    Submit-Slack-Post "Projectors lamp info: P1L1: $p1_l1_hm P1L2: $p1_l2_hm P2L1: $p2_l1_hm P2L2: $p2_l2_hm"
}
