#!/bin/bash -e

###
### Configuration
###

slack_webhook_url=""

projector1_ip=""
projector1_username=""
projector1_password=""

projector2_ip=""
projector2_username=""
projector2_password=""

log_file="./check-lamp-times.log"


###
### Utilities
###

function minutes_to_hm {
    ((h=${1}/60))
    ((m=${1}%60))
    printf "%02d:%02d\n" $h $m
}

function minutes_to_hundreds_of_hours {
    ((h=${1}/6000))
    echo "$h"
}

function slack_post {
    feed_text="{\"text\":\"$1\"}"
    curl -X POST \
         -H "Content-type: application/json" \
         -d "$feed_text" \
         "$slack_webhook_url"
}

function get_projector_info {
    ip="$1"
    login="$2"
    password="$3"
    curl -X POST \
         -c cookies.txt \
         -d "name=$login" \
         -d "pass=$password" \
         -d 'SET=LOGIN' \
         "http://$ip/cgi-bin/html.cgi" 2>/dev/null >/dev/null
    sleep 1
    curl -b cookies.txt \
         "http://$ip/cgi-bin/web.cgi?power=q&err=q&input=q&temp=q&lamp_info=q&lamp_mode=q&display(mute=q)&_='" \
         2>/dev/null
}

function parse_lamp_info_line {
    grep lamp_info
}

function parse_lamp1_time {
    if [[ "$1" =~ lamp=\"([0-9]+)\" ]]; then
        echo ${BASH_REMATCH[1]}
    fi
}

function parse_lamp2_time {
    if [[ "$1" =~ lamp2=\"([0-9]+)\" ]]; then
        echo ${BASH_REMATCH[1]}
    fi
}


## Main
##
error_count=0
if [[ -e "$log_file" ]]; then
    . "$log_file"
fi


projector_info1=$(get_projector_info "$projector1_ip" \
                                     "$projector1_username" \
                                     "$projector1_password")
lamp_info1=$(echo "$projector_info1" | parse_lamp_info_line)
p1_l1_time=$(parse_lamp1_time "$lamp_info1")
p1_l2_time=$(parse_lamp2_time "$lamp_info1")


projector_info2=$(get_projector_info "$projector2_ip" \
                                     "$projector2_username" \
                                     "$projector2_password")
lamp_info2=$(echo "$projector_info2" | parse_lamp_info_line)
p2_l1_time=$(parse_lamp1_time "$lamp_info2")
p2_l2_time=$(parse_lamp2_time "$lamp_info2")

p1_l1_hm=$(minutes_to_hm $p1_l1_time)
p1_l2_hm=$(minutes_to_hm $p1_l2_time)
p2_l1_hm=$(minutes_to_hm $p2_l1_time)
p2_l2_hm=$(minutes_to_hm $p2_l2_time)

if [[ "$lamp_info1" =~ get=\"err\" ]] || \
       [[ "$lamp_info2" =~ get=\"err\" ]]; then
    ((error_count++))
else
    error_count=0
fi


echo "Projector 1:"
echo "  Lamp1 Time: $p1_l1_hm"
echo "  Lamp2 Time: $p1_l2_hm"
echo "Projector 2:"
echo "  Lamp1 Time: $p2_l1_hm"
echo "  Lamp2 Time: $p2_l2_hm"

p1_l1_htime=$(minutes_to_hundreds_of_hours $p1_l1_time)
p1_l2_htime=$(minutes_to_hundreds_of_hours $p1_l2_time)
p2_l1_htime=$(minutes_to_hundreds_of_hours $p2_l1_time)
p2_l2_htime=$(minutes_to_hundreds_of_hours $p2_l2_time)

if (( $error_count > 5 )); then
    slack_post "Projectors lamp info: error count: $error_count"
    exit
elif (( $error_count > 0 )); then
    exit
fi

cat > "$log_file" <<EOF
##
## Log created: $(date --iso-8601=seconds)
##
## Projector 1:
##   Lamp1 Time: $p1_l1_hm
##   Lamp2 Time: $p1_l2_hm
## Projector 2:
##   Lamp1 Time: $p2_l1_hm
##   Lamp2 Time: $p2_l2_hm
##
prev_p1_l1_htime=$p1_l1_htime
prev_p1_l2_htime=$p1_l2_htime
prev_p2_l1_htime=$p2_l1_htime
prev_p2_l2_htime=$p2_l2_htime
error_count=$error_count
EOF

if [[ ! -e "$log_file" ]] \
       || [[ $p1_l1_htime != $prev_p1_l1_htime ]] \
       || [[ $p1_l2_htime != $prev_p1_l2_htime ]] \
       || [[ $p2_l1_htime != $prev_p2_l1_htime ]] \
       || [[ $p2_l2_htime != $prev_p2_l2_htime ]] \
   ; then
    ## log it!

    slack_post "Projectors lamp info: P1L1: $p1_l1_hm P1L2: $p1_l2_hm P2L1: $p2_l1_hm P2L2: $p2_l2_hm"
fi
